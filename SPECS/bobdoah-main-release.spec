%define release 2%(/usr/lib/rpm/redhat/dist.sh)

Name:           bobdoah-main-release
Version:        7
Release:        %{release}
Summary:        Bobdoah main repository configuration
Group:          System Environment/Base
License:        GPLv2
URL:            https://gitlab.com/bobdoah/bobdoah-main-release
Source0:        RPM-GPG-KEY-BOBDOAH-MAIN-7
Source1:        GPL
Source2:        bobdoah-main.repo
BuildArch:      noarch
Requires:       redhat-release >=  %{version}

%description
This package contains the Bobdoah EPEL repository
GPG key as well as configuration for yum.

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} .
install -pm 644 %{SOURCE1} .

%build

%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/%{SOURCE0}
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc GPL
%config(noreplace) /etc/yum.repos.d/*
/etc/pki/rpm-gpg/*

%changelog
* Tue Dec 18 2018 robertwilliams1985@gmail.com
- Initial packaging
